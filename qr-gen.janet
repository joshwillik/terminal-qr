(def all-filled "\u2588")
(def top-filled "\u2580")
(def bottom-filled "\u2584")
(def none-filled " ")
(def on true)
(def off false)

(defn unicode-cell [top bottom]
    (if (and top bottom)
        all-filled
        (if top
            top-filled
            (if bottom
                bottom-filled
                none-filled))))

(defn chunked [v n]
    (def out @[])
    (var i 0)
    (while (< i (length v))
        (array/push out (slice v i (min (+ i n) (length v))))
        (set i (+ i n)))
    out)

(defn zip [a b]
    (def out @[])
    (var i 0)
    (while (< i (length a))
        (array/push out [(get a i) (get b i)])
        (set i (+ i 1)))
    out)

(defn to-ascii [lines]
  (map
   (fn [line] (map
               (fn [char] (if char "1" "0"))
               line))
   lines))

(defn to-unicode [v]
    (map (fn [lineset]
            (def [top bottom] lineset)
            (map (fn [pair]
                (apply unicode-cell pair)) (zip top bottom)))
        (chunked v 2)))

(defn print-grid [lines]
    (print (string/join (map (fn [line] (string/join line "")) lines) "\n")))

(defn grid [width height v] {
    :width width
    :height height
    :data (array/new-filled (* width height) v)
})

(defn draw-grid [width & ops]
    (def pixels @[])
    (defn do-op [op]
        (if (struct? op)
            (do
                (def {:x x :y y :v v} op)
                (put pixels (+ (* y width) x) v))
            (map do-op op)
        )
    )
    (do-op ops) 
    (chunked pixels width))

(defn cell [x y v]
    {:x x :y y :v v})

(defn square [x y w v]
    (def out @[])
    (loop [x :range [x (+ x w)] y :range [y (+ y w)]]
        (array/push out (cell x y v))) 
    out)

(defn position [&keys {:x x :y y}] [
    (square x y 7 on)
    (square (+ x 1) (+ y 1) 5 off)
    (square (+ x 2) (+ y 2) 3 on)
])

(defn alignment [&keys {:x x :y y}] [
    (square x y 5 on)
    (square (+ x 1) (+ y 1) 3 off)
    (square (+ x 2) (+ y 2) 1 on)
])

(defn half [x] (math/floor (/ x 2)))

(defn deep-map [f xs]
    (map (fn [x]
        (if (tuple? x)
            (deep-map f x)
            (f x))) xs))

(defn main [& args]
    (def v 5)
    (def n (+ (* 4 v) 17))
    (def qr (draw-grid n
        (position :x 0 :y 0)
        (position :x 0 :y (- n 7))
        (position :x (- n 7) :y 0)

        (alignment :x (- (half n) 2) :y 5)
        (alignment :x 4 :y (- (half n) 2))
        (alignment :x (- (half n) 2) :y (- (half n) 2))
        (alignment :x (- n (+ 5 4)) :y (- (half n) 2))
        (alignment :x (- (half n) 2) :y (- n (+ 5 4)))
        (alignment :x (- n (+ 5 4)) :y (- n (+ 5 4)))
    ))
    (print-grid (to-unicode qr)))
