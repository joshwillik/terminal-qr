(declare-project
    :name "qr-gen"
    :version "0.1.0"
    :description "A QR code generator")

(declare-executable
    :name "qr-gen"
    :entry "qr-gen.janet")
